<?php

namespace Tests\Feature\TinyUrl\Services;

use App\Dto\ShortenedResponseDTO;
use App\Services\ShortUrlInterface;
use App\TinyUrl\Responses\Data;
use App\TinyUrl\Responses\TinyUrlResponse;

class MockTinyUrlClient implements ShortUrlInterface {

    public function create(string $url): ShortenedResponseDTO
    {
        return new TinyUrlResponse(
            new Data('http://example.com', 'https://tinyurl.com/4n7vyf77'),
            0,
            []
        );
    }
}