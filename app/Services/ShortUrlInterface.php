<?php

declare(strict_types=1);

namespace App\Services;

use App\Dto\ShortenedResponseDTO;

interface ShortUrlInterface {

    public function create(string $url): ShortenedResponseDTO;

}