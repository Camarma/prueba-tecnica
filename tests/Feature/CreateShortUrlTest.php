<?php

namespace Tests\Feature;

use App\Services\ShortUrlInterface;
use Tests\Feature\TinyUrl\Services\MockTinyUrlClient;
use Tests\TestCase;

class CreateShortUrlTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->app->bind(ShortUrlInterface::class, MockTinyUrlClient::class);
    }

    public function test_should_short_url(): void
    {   
        $request = [
            'url' => 'http://example.com'
        ];

        $this->withToken('{([])}');
        $response = $this->post('/api/v1/short-urls', $request);

        $url = json_decode($response->getContent());

        $response->assertStatus(200);
        $response->assertJsonStructure(['url']);
        $this->assertNotFalse(filter_var($url->url, FILTER_VALIDATE_URL));
    }

    public function test_should_unauthorized_request(): void
    {
        $request = [
            'url' => 'http://example.com'
        ];

        $response = $this->post('/api/v1/short-urls', $request);

        $response->assertStatus(401);
    }
}
