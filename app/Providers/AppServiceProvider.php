<?php

namespace App\Providers;

use App\Services\ShortUrlInterface;
use App\TinyUrl\Services\TinyUrlClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(
            ShortUrlInterface::class,
            TinyUrlClient::class
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
