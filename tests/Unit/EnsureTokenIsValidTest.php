<?php

namespace Tests\Unit;

use App\Http\Middleware\EnsureTokenIsValid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PHPUnit\Framework\TestCase;

class EnsureTokenIsValidTest extends TestCase
{
    /**
     * @dataProvider getValidTokens
     */
    public function test_ensure_token_is_valid(string $token): void
    {
        $request = $this->createMock(Request::class);

        $request->expects($this->once())
            ->method('bearerToken')
            ->willReturn($token);

        $ensureTokenValid = new EnsureTokenIsValid();

        $response = $ensureTokenValid->handle(
            $request,
            function () { return new Response();}
        );

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @dataProvider getInvalidTokens
     */
    public function test_ensure_token_is_invalid(?string $token): void
    {
        $request = $this->createMock(Request::class);

        $request->expects($this->once())
            ->method('bearerToken')
            ->willReturn($token);

        $ensureTokenValid = new EnsureTokenIsValid();

        $response = $ensureTokenValid->handle(
            $request,
            function () {}
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public static function getInvalidTokens(): array
    {
        return [
            [null],
            ["{a}"],
            ["{)"],
            ["[{]}"],
            ["((((()"],
        ];
    }

    public static function getValidTokens(): array
    {
        return [
            [''],
            ['{}'],
            ["{}[]()"],
            ["{([])}"],
        ];
    }
}
