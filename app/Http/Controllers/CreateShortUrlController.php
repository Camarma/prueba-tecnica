<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\ShortUrlRequest;
use App\TinyUrl\Exceptions\TinyUrlException;
use App\UseCases\ShortUrlUseCase;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CreateShortUrlController extends Controller
{
    public function __construct(public ShortUrlUseCase $shortUrlUseCase)
    {

    }

    public function __invoke(ShortUrlRequest $request): JsonResponse
    {
        try {
            $request->validated();

            $url = $this->shortUrlUseCase->__invoke($request->get('url'));

            return new JsonResponse(['url' => $url]);
        } catch(TinyUrlException $exception) {
            return (new JsonResponse(
                $exception->getMessage(),
                Response::HTTP_BAD_REQUEST
            ));
        } catch(Exception $exception) {
            return (new JsonResponse(
            'Internal server error',
            Response::HTTP_INTERNAL_SERVER_ERROR
            ));
        }
    }
}
