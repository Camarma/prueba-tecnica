<?php

declare(strict_types=1);

namespace App\UseCases;

use App\Services\ShortUrlInterface;

class ShortUrlUseCase
{
    public function __construct(public ShortUrlInterface $shortUrl)
    {
    }

    public function __invoke(string $url): string
    {
        $shortedResponse = $this->shortUrl->create($url);

        return $shortedResponse->getUrl();
    }
}