<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Technical test

### Installation

To get started, follow these steps:

1. Copy *.env.example* to create the *.env* file 

2. Set a value on *TINYURL_API_TOKEN* using the api token created at https://tinyurl.com/app/settings/api

3. Initialize the Docker containers:

    ```bash
    docker compose up
    ```
   
### Api endpoint

<span style="color: orange">POST</span> http://localhost/api/v1/short-urls
    
    curl --location --request POST 'http://localhost/api/v1/short-urls' \
    --header 'Authorization: Bearer {}' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "url": "http://example.com"
    }'

### Running the test

    docker exec -it php-fpm-prueba-tecnica /bin/bash
    php artisan test 