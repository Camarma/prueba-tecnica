<?php

declare(strict_types=1);

namespace App\TinyUrl\Exceptions;

use Exception;

class TinyUrlException extends Exception {

    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}