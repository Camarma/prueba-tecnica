<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class EnsureTokenIsValid
{
    private const VALID_OPEN_PUNCTUATION_MARKS = ['[', '(', '{'];
    private const VALID_CLOSE_PUNCTUATION_MARKS = [']', ')', '}'];

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->bearerToken();

        if (!$token && $token !== '') {
            return new JsonResponse(null, 401);
        }

        return $this->isValidToken($token) ? $next($request) : new JsonResponse('Unauthorized', 401);
    }

    private function isValidToken(string $token): bool
    {
        $splitToken = str_split($token);
        $associatePunctuationMarks = array_combine(self::VALID_OPEN_PUNCTUATION_MARKS, self::VALID_CLOSE_PUNCTUATION_MARKS);
        $stackPunctuationMarks = [];

        foreach ($splitToken as $punctuationMark) {
            if (!$this->isValidPunctuationMark($punctuationMark)) {
                return false;
            }

            if ($this->isValidOpenPunctuationMark($punctuationMark)) {
                $stackPunctuationMarks[] = $punctuationMark;
                continue;
            }

            $lastPunctuationMark = array_pop($stackPunctuationMarks);
            if (!isset($associatePunctuationMarks[$lastPunctuationMark]) ||
                $associatePunctuationMarks[$lastPunctuationMark] !== $punctuationMark
            ) {
                return false;
            }
        }

        return !count($stackPunctuationMarks);
    }

    private function isValidOpenPunctuationMark(string $punctuationMark): bool
    {
        return in_array($punctuationMark, self::VALID_OPEN_PUNCTUATION_MARKS);
    }

    private function isValidClosePunctuationMark(string $punctuationMark): bool
    {
        return in_array($punctuationMark, self::VALID_CLOSE_PUNCTUATION_MARKS);
    }

    private function isValidPunctuationMark(string $punctuationMark): bool
    {
        return $this->isValidOpenPunctuationMark($punctuationMark) || $this->isValidClosePunctuationMark($punctuationMark);
    }
}
