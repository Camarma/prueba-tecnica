<?php

declare(strict_types=1);

namespace App\TinyUrl\Responses;

use App\Dto\ShortenedResponseDTO;

class TinyUrlResponse implements ShortenedResponseDTO {

    public function __construct(public Data $data,
        public int $code,
        public array $errors)
    {
    }

    public function getUrl(): string
    {
        return $this->data->getTinyUrl();
    }
}