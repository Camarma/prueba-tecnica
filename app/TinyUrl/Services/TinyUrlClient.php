<?php

declare(strict_types=1);

namespace App\TinyUrl\Services;

use App\Dto\ShortenedResponseDTO;
use App\Services\ShortUrlInterface;
use App\TinyUrl\Exceptions\TinyUrlException;
use App\TinyUrl\Responses\Data;
use App\TinyUrl\Responses\TinyUrlResponse;
use Illuminate\Support\Facades\Http;

class TinyUrlClient implements ShortUrlInterface {

    public function create(string $url): ShortenedResponseDTO
    {
        $httpResponse = Http::withToken(env('TINYURL_API_TOKEN'))
        ->post(env('TINYURL_API_URL') . '/create', ['url' => $url]);

        $response = $httpResponse->json();

        $data = $response['data'];
        $code = $response['code'];
        $error = $response['errors'];

        if (!$httpResponse->successful()) {
            throw new TinyUrlException('Tinyurl error code: ' . $code);
        }

        return new TinyUrlResponse(
            new Data($data['tiny_url'], $data['url']),
            $code,
            $error
        );
    }
}