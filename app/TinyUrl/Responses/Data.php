<?php

declare(strict_types=1);

namespace App\TinyUrl\Responses;

class Data {
    
    public function __construct(private string $tinyUrl, private string $url)
    {
    }

    public function getTinyUrl(): string
    {
        return $this->tinyUrl;
    }
}