<?php

namespace Tests\Feature;

use App\TinyUrl\Exceptions\TinyUrlException;
use App\TinyUrl\Services\TinyUrlClient;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class TinyUrlClientTest extends TestCase
{
    public function test_should_return_a_tinyurl(): void
    {
        $expectedTinyUrl = 'https://tinyurl.com/12345';
        $url = 'https://example.com';
        $sut = new TinyUrlClient();

        Http::fake(Http::response(
            [
                'data' =>
                    [
                        'url' => $url, 'tiny_url' => 'https://tinyurl.com/12345'
                    ],
                'code' => 0,
                'errors' => []
            ]
        ));

        $response = $sut->create($url);

        $this->assertEquals($expectedTinyUrl, $response->getUrl());
    }

    public function test_should_not_return_a_tinyurl(): void
    {
        $sut = new TinyUrlClient();

        Http::fake(Http::response(['data' => [], 'code' => 1, 'errors' => ['Unauthorized']],401));

        $this->expectException(TinyUrlException::class);

        $sut->create('https://example.com');
    }
}
